# Resolver conflictos en merge request

Muchas veces se necesitan hacer modificaciones pequeñas a los archivos que se entregaron para poder calificar las actividades.
Esto puede ocasionar conflictos con los archivos de entregas pasadas, mismos que se deben de solucionar antes de poder aceptar la nueva entrega.

## Actualizar la rama del merge request

- Abrir la página donde se listan los [_merge requests_][merge-requests] disponibles para la entrega en curso.

- Seleccionar el merge request donde se está entregando la actividad.

- Dar clic en el botón **editar**.

| ![](img/update-001-edit-merge-request.png) |
|:------------------------------------------:|
| Editar _merge request_                     |

- Cambiar la rama de `entregas` a `entregados`.

| ![](img/update-002-change-branch.png) |
|:-------------------------------------:|
| Ajustando rama de _merge request_     |

- Dar clic en el botón **guardar cambios** en la parte inferior de la página.

- La rama de destino se cambió correctamente.

| ![](img/update-003-changes-saved.png)               |
|:---------------------------------------------------:|
| Se actualizó la rama de destino del _merge request_ |

[merge-requests]: https://gitlab.com/Redes-Ciencias-UNAM/2023-1/tareas-redes/-/merge_requests

## Clonar repositorio central de tareas

Clonar el repositorio central de tareas.

```sh
$ git clone https://gitlab.com/Redes-Ciencias-UNAM/2023-1/tareas-redes.git repositorio-central
```

Entrar al directorio del `repositorio-central` y cambiar la rama a `entregados` y obtener las últimas actualizaciones de esa rama.

```sh
$ pushd repositorio-central

$ git checkout entregados

$ git pull
Already up to date.

$ popd
```

Listar el directorio, se deben mostrar dos carpetas:

- `repositorio-central`: Copia del repositorio central de tareas y prácticas.
- `tareas-redes`: Copia de trabajo donde se hacen las modificaciones y se trabajan las actividades.

```sh
$ ls
repositorio-central	tareas-redes
```

## Identificar archivos con conflictos

Abrir la página del merge request de la entrega en curso y revisar si existen conflictos.

| ![](img/update-004-merge-conflicts.png) |
|:---------------------------------------:|
| Conflictos en merge request |

Dar clic en el botón **Resolve conflicts** para visualizar los archivos que tienen conflictos y anotar las rutas de los archivos con problemas.

- No intentar resolver el conflicto en la interfaz web de git, esto hace muchas más acciones que las estríctamente necesarias.

| ![](img/update-005-view-merge-conflicts.png) |
|:--------------------------------------------:|
| Visualizar conflictos en merge request       |

Copiar los archivos con problemas desde el clon del **repositorio central** hacia el **repositorio del equipo**

| NOTA |
|:----:|
| Esto también se puede hacer desde el navegador de archivos |

- Listar los archivos afectados en el `repositorio-central` y en el copia de trabajo `tareas-redes`.

```sh
$ ls
repositorio-central	tareas-redes

$ ls -la repositorio-central/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
-rw-r--r--  1 tonejito  users  51 Nov 29 01:02 repositorio-central/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c

$ ls -la tareas-redes/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
-rw-r--r--  1 tonejito  users  81 Nov 30 03:04 tareas-redes/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
```
- Copiar los archivos con conflictos del clon del `repositorio-central` hacia el copia de trabajo `tareas-redes`.

    - Repetir para todos los archivos que tengan conflictos.

```sh
$ cp repositorio-central/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c \
               tareas-redes/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
```

- Revisar el estado en el copia de trabajo.

    - Cada archivo con conflictos se deberá mostrar como **modificado**.

```sh
$ cd tareas-redes

$ git status
On branch AndresHernandez
Your branch is up to date with 'origin/AndresHernandez'

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
```

- Opcionalmente revisar los cambios.

```sh
$ git diff docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
diff --git a/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c b/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
index 1a2b3c4..4d5e6f0 100644
--- a/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
+++ b/docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
@@ -1,3 +1,8 @@
 #include <stdio.h>

-int main(void)
+int main(int argc, char* argv)
 {
   return 0;
 }
```

- Agregar cada archivo modificado al _staging area_ para hacer un _commit_.

```sh
$ git add docs/entrega/practica-1/Equipo-AAAA-BBBB-CCCC-DDDD/programa.c
```

- Hacer un _commit_ para versionar los cambios y actualizar el repositorio en GitLab.

```sh
$ git commit -m "Cambios del repositorio central"

$ git push
```

- El _merge request_ se actualizará y los conflictos se resolverán, porque los archivos tendrán el mismo contenido.

## Notas

- No agregar archivos que no hayan sido hechos por el equipo, esto causa aún más conflictos en el repositorio central.
- No borrar los archivos con conflictos, esto causa problemas en el repositorio central.
- No intentar resolver el conflicto en la interfaz web de git, esto hace muchas más acciones que las estríctamente necesarias.
